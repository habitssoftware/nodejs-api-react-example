## Introduction ##
This project serves as an example of a project with NodeJS API and React. It contains both the API and app in the same repository. All API files can be found in the api folder. The files for running the React app can be found in most other folders. 
 
## Getting started ##
- Install dependencies using `yarn`.
- Start MongoDB by running `mongod&`. 
- Start the API server by running `yarn start-server`.
- Start the app by either running the development environment with `yarn start` or by building it with `yarn build`.

Make sure that your local database has been populated with the provided ruleset. Populating can be done using `yarn populate`.

## Team ##
- *Developer*: Richard van Hees